//
//  MyRepViewController.swift
//  Congressional Directory
//
//  Created by Prince Dupea on 11/15/17.
//  Copyright © 2017 Prince Dupea. All rights reserved.
//

import Foundation
import UIKit
import CoreData
import SDWebImage


class MyRepViewController: UITableViewController, NSFetchedResultsControllerDelegate {
	// sort the data into setions -> Senators and Representative
    let sectionSort = NSSortDescriptor(key: "title", ascending: true)
    let firstNameSort = NSSortDescriptor(key: "firstName", ascending: true)
    var fetchedResultsController: NSFetchedResultsController<NSFetchRequestResult>!
	
	// show only reps for their home state
    let predicate = NSPredicate(format: "stateAbbreviation = %@", "\(UserDefaults.standard.value(forKey: STATE_ABREV)!)")
    var sectionTitles = ["Representatives", "Senators"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
		if (!UserDefaults.standard.bool(forKey: COMMITTEES_MEMBERSHIP)) {
			DataFetcher.updateCommitteeInfo()
			UserDefaults.standard.set(true, forKey: COMMITTEES_MEMBERSHIP)
		}
        fetchedResultsController = FRC.initFRC(MEMBER_ENTITY, [sectionSort, firstNameSort], predicate, self)
        tableView.tableFooterView = UIView()
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		// Set up the cell
        let cell = tableView.dequeueReusableCell(withIdentifier: CELL_IDENTIFIER, for: indexPath) as! RepresentativeCellView
        guard let object = self.fetchedResultsController?.object(at: indexPath) else {
            fatalError("Attempt to configure cell without a managed object")
        }
		
        let member = object as! CongressMember
		cell.name.text = member.firstName! + " " + member.middleName! + " " + member.lastName!
		let imageUrl: URL = URL(string: IMAGE_BASE_URL + member.apiID! + ".jpg")!
		cell.repImage.sd_setIndicatorStyle(.whiteLarge)
		cell.repImage.sd_setImage(with: imageUrl, placeholderImage: UIImage(named: "placeholder.png"))
		
		if (member.title == "Representative")
		{
			let rep = member as! Representative
			if(member.party == "R")
			{
				if(rep.districtAtLarge)
				{
					cell.party.text = "Republican-" + "(District \(DISTRICT_AT_LARGE))"
				} else
				{
					cell.party.text = "Republican-" + "(District \(rep.district))"
				}
			} else
			{
				if(rep.districtAtLarge)
				{
					cell.party.text = "Democrat-" + "(District \(DISTRICT_AT_LARGE))"
				} else
				{
					cell.party.text = "Democrat-" + "(District \(rep.district))"
				}
			}
		} else
		{
			if(member.party == "R")
			{
				cell.party.text = "Republican"
			} else
			{
				cell.party.text = "Democrat"
			}
		}
        return cell
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return fetchedResultsController.sections!.count
    }
	
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sectionTitles[section]
    }
	
	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		let selected = fetchedResultsController.object(at: indexPath) as! CongressMember
		SELECTED_MEMBER_ID = selected.apiID!
		if (!selected.infoIsUpToDate) {
			// retrive addtional information about this member
			DataFetcher.updateInfoFor(member: selected)
			DataFetcher.getLegislation(memberID: SELECTED_MEMBER_ID, billType: BILLS_INTRODUCED, cascade: true)
		}
	}

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let sections = fetchedResultsController.sections else {
            fatalError("No sections in fetchedResultsController")
        }
        let sectionInfo = sections[section]
		if (sectionInfo.numberOfObjects == 1) {
			sectionTitles[0] = "Representative"
		} else {
			sectionTitles[0] = "Representatives"
		}
        return sectionInfo.numberOfObjects
    }
}
