//
//  ContactsTVC.swift
//  Congressional Directory
//
//  Created by Prince Dupea on 12/8/17.
//  Copyright © 2017 Prince Dupea. All rights reserved.
//

import UIKit
import CoreData

class ContactsTVC: UITableViewController, NSFetchedResultsControllerDelegate {
	private let CELL_ID = "location"
	var frc: NSFetchedResultsController<NSFetchRequestResult>!
	
	override func viewDidLoad() {
        super.viewDidLoad()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
		initializeFetchedResultsController()
		// hide empty row
		tableView.tableFooterView = UIView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return frc.sections!.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		guard let sections = frc.sections else {
			fatalError("[ContactsTVC]No sections in fetchedResultsController")
		}
		let sectionInfo = sections[section]
		return sectionInfo.numberOfObjects
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CELL_ID, for: indexPath)
		guard let object = frc?.object(at: indexPath) else {
			fatalError("[ContactsTVC]Attempt to configure cell without a managed object")
		}
        // Configure the cell...
		let location = object as! Office
		cell.textLabel?.text = location.address!
		cell.detailTextLabel?.text = location.hours!
        return cell
    }
	
	override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
		guard let sections = frc.sections else {
			fatalError("[ContactsTVC]No sections in fetchedResultsController")
		}
		let sectionInfo = sections[section]
		return sectionInfo.name
	}
	
	override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		let selectedOffice = frc.object(at: indexPath) as! Office
		SELECTED_OFFICE_ID = selectedOffice.officeID!
	}

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
	func initializeFetchedResultsController() {
		let request = NSFetchRequest<NSFetchRequestResult>(entityName: OFFICE_ENTITY)
		let selectBaseOn = NSSortDescriptor(key: "officeID", ascending: true)
		let predicate = NSPredicate(format: "ANY officeOf.apiID = %@", "\(SELECTED_MEMBER_ID)")
		request.sortDescriptors = [selectBaseOn]
		request.predicate = predicate
		
		let moc = CDManager.context()
		frc = NSFetchedResultsController(fetchRequest: request, managedObjectContext: moc, sectionNameKeyPath: "city", cacheName: nil)
		frc.delegate = self
		
		do {
			try frc.performFetch()
		} catch {
			fatalError("Failed to initialize FetchedResultsController: \(error)")
		}
	}
}
