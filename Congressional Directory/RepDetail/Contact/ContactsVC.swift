//
//  ContactsVC.swift
//  Congressional Directory
//
//  Created by Prince Dupea on 12/8/17.
//  Copyright © 2017 Prince Dupea. All rights reserved.
//

import UIKit

class ContactsVC: UIViewController {
	var url = ""
	@IBOutlet weak var contactFormBtn: UIButton!
	@IBOutlet weak var youtubeBtn: UIButton!
	@IBOutlet weak var twitterBtn: UIButton!
	@IBOutlet weak var facebookBtn: UIButton!
	
	override func viewDidLoad() { super.viewDidLoad() }

	@IBAction func contactFormBtnAction(_ sender: UIButton) {
		let selected:CongressMember = CDManager.fetchMember(SELECTED_MEMBER_ID)
		switch sender.titleLabel?.text {
		case " Email"?:
			if (selected.contacts?.contactForm != nil)
			{
				self.url = (selected.contacts?.contactForm!)!
				let myURL = URL(string: url)
				UIApplication.shared.open(myURL!, options: [:]) { _ in }
			} else {
				if (selected.title == "Senator")
				{
					let sen = selected as! Senator
					showAlert(message: "OOPS! \(sen.shortTitle!) \(sen.lastName!)'s contact form is unavailable within the app.", title: "No contact form")
					
				} else
				{
					let sen = selected as! Representative
					showAlert(message: "OOPS! \(sen.shortTitle!) \(sen.lastName!)'s contact form is unavailable within the app.", title: "No contact form")
				}
			}
		case " Facebook"?:
			if (selected.contacts?.facebookAccount != nil)
			{
				self.url = "https://facebook.com/" + (selected.contacts?.facebookAccount!)!
				let myURL = URL(string: url)
				UIApplication.shared.open(myURL!, options: [:]) { _ in }
			} else {
				if (selected.title == "Senator")
				{
					let sen = selected as! Senator
					showAlert(message: "OOPS! \(sen.shortTitle!) \(sen.lastName!) does not have a Facebook page.", title: "No Facebook page")
					
				} else
				{
					let sen = selected as! Representative
					showAlert(message: "OOPS! \(sen.shortTitle!) \(sen.lastName!) does not have a Facebook page.", title: "No Facebook page")
				}
			}
		case " Twitter"?:
			if (selected.contacts?.twitterAccount != nil)
			{
				self.url = "https://twitter.com/" + (selected.contacts?.twitterAccount!)!
				let myURL = URL(string: url)
				UIApplication.shared.open(myURL!, options: [:]) { _ in }
			} else {
				if (selected.title == "Senator")
				{
					let sen = selected as! Senator
					showAlert(message: "OOPS! \(sen.shortTitle!) \(sen.lastName!) does not have a Twitter account.", title: "No Twitter account")
					
				} else
				{
					let sen = selected as! Representative
					showAlert(message: "OOPS! \(sen.shortTitle!) \(sen.lastName!) does not have a Twitter account.", title: "No Twitter account")
				}
			}
		case " YouTube"?:
			if (selected.contacts?.youtubeAccount != nil)
			{
				self.url = "https://youtube.com/user/" + (selected.contacts?.youtubeAccount!)!
				let myURL = URL(string: url)
				UIApplication.shared.open(myURL!, options: [:]) { _ in }
			} else {
				if (selected.title == "Senator")
				{
					let sen = selected as! Senator
					showAlert(message: "OOPS! \(sen.shortTitle!) \(sen.lastName!) does not have a YouTube channel.", title: "No YouTube channel")
					
				} else
				{
					let sen = selected as! Representative
					showAlert(message: "OOPS! \(sen.shortTitle!) \(sen.lastName!) does not have a YouTube channel.", title: "No YouTube channel")
				}
			}
		default: break
		}
	}
	
	func showAlert(message: String, title: String) {
		// create the alert
		let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
		
		// add an action (button)
		alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.default, handler: nil))
		
		// show the alert
		self.present(alert, animated: true, completion: nil)
	}
	// MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
		if (segue.identifier == "toContactFormWebView") {
			let dest = segue.destination as! CongressionalWebsiteVC
			dest.memberContactFromString = url
		}
    }

}
