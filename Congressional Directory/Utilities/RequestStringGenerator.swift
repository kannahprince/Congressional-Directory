//
//  RequestStringGenerator.swift
//  Congressional Directory
//
//  Created by Prince Dupea on 12/14/17.
//  Copyright © 2017 Prince Dupea. All rights reserved.
//

import Foundation

class RequestStringGenerator {
	
	class func craftRequestForCommitteMembers(_ committeeID:String, _ chamber: String) -> String {
		var str = API_BASE_URL
		str += "\(CURRENT_API_VERSION)/\(CURRENT_CONGRESS)/\(chamber)/committees/\(committeeID).json"
		return str
	}
	
	class func craftRequestFromBills(by memberID: String, billType: String) -> String {
		var str = API_BASE_URL
		str += "\(CURRENT_API_VERSION)/members/\(memberID)/bills/\(billType).json"
		return str
	}
	
	class func craftRequestForMember(_ memberID:String) -> String {
		var str = API_BASE_URL
		str += "\(CURRENT_API_VERSION)/members/\(memberID).json"
		return str
	}
	
	class func craftRequestForCommitteesIn(_ chamber:String, joint:Bool = false, congress:Int = CURRENT_CONGRESS) -> String {
		var str = API_BASE_URL
		if (joint)
		{
			str += "\(CURRENT_API_VERSION)/\(congress)/joint/committees.json"
		} else
		{
			str += "\(CURRENT_API_VERSION)/\(congress)/\(chamber.lowercased())/committees.json"
		}
		return str
	}
	
	class func craftRequestForAllMembers(_ chamber:String, congress:Int = CURRENT_CONGRESS) -> String {
		var str = API_BASE_URL
		str += "\(CURRENT_API_VERSION)/\(congress)/\(chamber.lowercased())/members.json"
		return str
	}
}
