# Congressional Directory

`Congresstional Directroy (CD)` is an iOS app that allwos you to see a wide array of data points about your
congressional representatives in Washington, DC.

Data is pulled from [ProPublica Congress API](https://projects.propublica.org/api-docs/congress-api/). Addtional information is pulled from the [congress-legislators project](https://github.com/unitedstates/congress-legislators). Member photos are scrapped from the [Biographical Directory of the United States Congress](http://bioguide.congress.gov/biosearch/biosearch.asp).

# Running CD

You'll need a Mac computer with [Xcode 9 or higer](https://itunes.apple.com/us/app/xcode/id497799835?mt=12).

This projects uses [CocoaPods](https://cocoapods.org) for dependency management. 
If you have that install then clone the repo, run `pod install` in the root of 
the project. Once that is complete open the workspace (`.xcworkspace`) file in 
Xcode and hit the play button.

You'll need a device running iOS 10 or later if you want to run CD on a physical iOS device.

# Screen shots:

### Menu/Navigation:

![Home screen](screenshots/home.png)  ![Home screen and menu](screenshots/home_menu.png) 

### Representative details:

![Representatives](screenshots/reps.png)  ![Representative bio](screenshots/rep_bio.png)

![Representative contacts](screenshots/rep_contacts.png)  ![Representative district offices](screenshots/rep_contact_district_offices.png)

![Representative district office detail](screenshots/rep_contacts_district_office_detail.png)  ![Representative legislation histroy](screenshots/rep_legislation.png)


![Representative committees](screenshots/rep_committees.png)

### Robust representative search with scoping:

![All search results](screenshots/search_all.png) ![Scope Senate](screenshots/search_senate.png) 

![Scope House](screenshots/search_house.png)


### Committee detail:

![Congressional committees](screenshots/committees.png)  ![Committee members](screenshots/committees_members.png)


![Committee subcommittees](screenshots/committees_sub.png)